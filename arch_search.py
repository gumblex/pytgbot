import requests

def arch_search(query):
    query = query.lower() # special for the stupid tg modules...

    # stable repo
    official_url = "https://www.archlinux.org/packages/search/json/?name={}&repo=Community&repo=Extra&repo=Core&repo=Multilib".format(query)
    AUR_url = "https://aur.archlinux.org/rpc/?v=5&type=search&arg={}".format(query)
    official_yaourt = requests.get(official_url)
    official_yaourt_json = official_yaourt.json()

    AUR_yaourt = requests.get(AUR_url)
    AUR_yaourt_json = AUR_yaourt.json()

    if official_yaourt_json['results'] != []:
        json_result = official_yaourt_json['results'][0]
        Name = json_result['pkgname']
        Version = json_result['pkgver']
        Desc = json_result['pkgdesc']
        Repo = json_result['repo']
        results = '''
        Name: {}
        Version: {}
        Description: {}
        Repo: {}
        '''.format(Name, Version, Desc, Repo)
        return results

    elif AUR_yaourt_json['resultcount'] != 0:
        json_result = AUR_yaourt_json['results'][0]
        Name = json_result['Name']
        Version = json_result['Version']
        Desc = json_result['Description']
        results = '''
        Name: {}
        Version: {}
        Desc: {}
        Repo: AUR
        '''.format(Name, Version, Desc)
        return results

# debug
