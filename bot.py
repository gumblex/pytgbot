#!/usr/bin/env python3

import telegram
from telegram.ext import Updater
from telegram.ext import CommandHandler
import logging
import json

from arch_search import arch_search
from google_search import google_search

# for log
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

'''
google API
AIzaSyCi-jHl_QN7JoV5oh4Mpmgmu2coS_sO8G4
'''

# token
with open('config.json', 'r') as f:
    bot_token = json.load(f)

bot = Updater(token = bot_token['token'])
dispatcher = bot.dispatcher

# bot action
def start(bot, update):
    bot.sendMessage(chat_id = update.message.chat_id, text = 'Hi~ 咱是夏洛克狼的bot~')

def google(bot, update, args):
    query = ' '.join(args).upper()
    result = google_search(query)
    bot.sendMessage(chat_id = update.message.chat_id, text = result)

def yaourt(bot, update, args):
    query = ' '.join(args).upper()
    result = arch_search(query)
    bot.sendMessage(chat_id = update.message.chat_id, text = result)

# run bot
start_handler = CommandHandler('start', start)
google_handler = CommandHandler('google', google, pass_args = True)
yaourt_handler = CommandHandler('yaourt', yaourt, pass_args = True)

dispatcher.add_handler(start_handler)
dispatcher.add_handler(google_handler)
dispatcher.add_handler(yaourt_handler)

bot.start_polling()

# debug
